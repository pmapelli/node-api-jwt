const mongoose = require('mongoose');

// mongoose.connect('mongodb://localhost/noderest', { useMongoClient: true});

 mongoose.createConnection('mongodb://localhost/noderest', 
    { useCreateIndex: true, useNewUrlParser: true});

// const uri = 'mongodb://localhost/noderest'
// mongoose.createConnection(uri, { useNewUrlParser: true })

mongoose.Promise = global.Promise;

module.exports = mongoose;